package converter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Main {

    private static Scanner scanner = new Scanner(System.in);
    private static ArrayList<String> alphabet = new ArrayList<>(Arrays.asList("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p",
            "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"));

    public static void main(String[] args) {

        int sourceRadix = 0;
        String num = "";
        int targetRadix = 0;

        try {
            sourceRadix = Integer.parseInt(scanner.next());
            num = scanner.next();
            targetRadix = Integer.parseInt(scanner.next());
        } catch (Exception e) {
            System.out.println("Error: please input valid numbers");
            return;
        }

        if(num.equals("1") || num.equals("2") || targetRadix <= 0) {
            System.out.println("Error: please input valid numbers");
            return;
        }

        convertIntegerPartToDecimal(sourceRadix, num, targetRadix);
        convertFractionalPartToDecimal(sourceRadix, num, targetRadix);
    }

    public static void convertIntegerPartToDecimal(int sourceRadix, String num, int targetRadix) {

        num = num.split("\\.")[0];

        if(sourceRadix == 1) {
            int number = Integer.parseInt(num);

            int count = 0;
            while(number > 0) {
                number /= 10;
                count++;
            }
            System.out.print(Long.toString(count, targetRadix));
        } else if(targetRadix == 1) {
            for(int i = 0; i < Integer.parseInt(num); i++) {
                System.out.print(1);
            }
        } else {
            System.out.print(Integer.toString(Integer.parseInt(num, sourceRadix), targetRadix));
        }
    }

    public static void convertFractionalPartToDecimal(int sourceRadix, String num, int targetRadix) {
        if(!num.contains(".")) return;

        String fractionalPart = num.split("\\.")[1];
        String[] fractionalPartStringArray = fractionalPart.split("");

        double firstFractionalPart = calculateFirstPart(fractionalPartStringArray, sourceRadix);
        calculateSecondPart(firstFractionalPart, targetRadix);
    }

    public static double calculateFirstPart(String[] fractionalPartStringArray, int sourceRadix) {

        double sum = 0.0;
        for(int i = 0; i < fractionalPartStringArray.length; i++) {

            int a = Integer.parseInt(String.valueOf(fractionalPartStringArray[i]), sourceRadix);
            sum += a / (double) Math.pow(sourceRadix, i+1);
        }

        return sum;
    }

    public static void calculateSecondPart(double fractionalPart, int targetRadix) {

        long firstPart;
        double lastPart = fractionalPart;
        double operation;
        StringBuilder finalNumber = new StringBuilder();

        for(int i = 0; i < 5; i++) {

            operation = lastPart * (long) targetRadix;
            firstPart = (long) operation;

            if(firstPart > 9) {
                for(int j = 0; j < alphabet.size(); j++) {
                    if(firstPart == j + 10) {
                        lastPart = operation - (j + 10);
                        finalNumber.append(alphabet.get(j));
                    }
                }
            } else {
                lastPart = operation - firstPart;
                finalNumber.append(firstPart);
            }
        }

        System.out.println("." + finalNumber);
    }
}