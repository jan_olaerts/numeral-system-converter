import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // put your code here
        double distanceInM = scanner.nextDouble();
        double travelTime = scanner.nextDouble();

        double averageSpeed = distanceInM / travelTime;
        System.out.println(averageSpeed);
    }
}